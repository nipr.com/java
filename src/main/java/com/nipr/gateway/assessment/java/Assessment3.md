# Assessment 3 - SQL

## Tables

*EMPLOYEES*

| employee_id | first_name | last_name | department | start_date | project_id |
|-------------|------------|-----------|------------|------------|------------|
| 1           | Sam        | Apple     | HR         | 1/1/2000   | 2          |
| 2           | Nicole     | Orange    | Finance    | 2/1/2001   | 2          |
| 3           | Alex       | Banana    | IS         | 3/1/2002   | 1          |
| 4           | Trevor     | Pear      | Marketing  | 4/1/2003   | 2          |
| 5           | Peter      | Mellon    | IS         | 5/1/2004   | 1          |

*PROJECTS*

| project_id | project_name  | status_id |
|------------|---------------|-----------|
| 1          | Data Cleanup  | 1         |
| 2          | Data Security | 2         |

*PROJECT_STATUSES*

| status_id | status_name |
|-----------|-------------|
| 1         | In Progress |
| 2         | Completed   |
| 3         | On Hold     |
| 4         | Cancelled   |

### Questions

1. On these tables please identify the primary keys and foreign keys

2. Write a query to pull back employee ID, first name, last name and project name ordered by project name

3. Can this query be improved, if so how?

    ```sql
    SELECT e.first_name, e.last_name
    FROM projects p, project_statuses s, employees e
    WHERE p.project_id = e.project_id
      AND p.project_name = 'DATA_CLEANUP'
    ```

4. Management would like to assign employees to a second project. What changes would you make to the data model to support this?