package com.nipr.gateway.assessment.java;

import java.util.Arrays;
import java.util.List;

public class Assessment1 {

    private static final String ADMIN_ROLE = "admin";

    public static void main(String[] args) {

        List<User> users = Arrays.asList(
                new User("John", "guest"),
                new User("Henry", "admin"),
                new User("Amanda", "poweruser")
        );

        //TODO for each user if user has the admin role print "Hello Admin ${name}" else print "Hello ${name}"

    }

    private static class User {
        private final String name;
        private final String role;


        private User(String name, String role) {
            this.name = name;
            this.role = role;
        }

        public String getName() { return name; }
        public String getRole() { return role; }
    }
}
