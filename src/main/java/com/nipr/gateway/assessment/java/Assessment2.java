package com.nipr.gateway.assessment.java;

import java.util.List;
import java.util.stream.Collectors;

import static com.nipr.gateway.assessment.java.Assessment2.State.CALIFORNIA;

public class Assessment2 {

    /**
     * Imagine reviewing this code in a code review.
     * What issues do you see with the code?
     * What questions would you have?
     */
    public boolean waSpecialRuleProducerDisabilityQualification(List<LicenseInfo> licenses) {
        boolean hasCAPropertyBrokerAgent = false;
        boolean hasCACasualtyBrokerAgent = false;
        boolean hasCAFireAndCasualtyBrokerAgent = false;

        for (LicenseInfo l : licenses.stream().filter(l -> CALIFORNIA.equals(l.state)).collect(Collectors.toList())) {
            switch(l.loaCode) {
                case "7132":
                    hasCAPropertyBrokerAgent = true;
                    break;
                case "7133":
                    hasCACasualtyBrokerAgent = true;
                    break;
                case "290":
                    hasCAFireAndCasualtyBrokerAgent = true;
                    break;
            }
        }

        return hasCAPropertyBrokerAgent && (hasCACasualtyBrokerAgent || hasCAFireAndCasualtyBrokerAgent);
    }

    public static class LicenseInfo {
        State state;
        String loaCode;
        String licenseClassCode;
    }


    public enum State {
        ALABAMA, ALASKA, ARIZONA, ARKANSAS, CALIFORNIA, COLORADO, CONNECTICUT, DELAWARE, DISTRICT_OF_COLUMBIA,  FLORIDA,
        GEORGIA, HAWAII, IDAHO, ILLINOIS, INDIANA, IOWA, KANSAS, KENTUCKY, LOUISIANA, MAINE, MARYLAND, MASSACHUSETTS,
        MICHIGAN, MINNESOTA, MISSISSIPPI, MISSOURI, MONTANA, NEBRASKA, NEVADA, NEW_HAMPSHIRE, NEW_JERSEY, NEW_MEXICO,
        NEW_YORK, NORTH_CAROLINA, NORTH_DAKOTA, OHIO, OKLAHOMA, OREGON, PENNSYLVANIA, PUERTO_RICO, RHODE_ISLAND,
        SOUTH_CAROLINA, SOUTH_DAKOTA, TENNESSEE, TEXAS, UTAH, VERMONT, VIRGINIA, WASHINGTON, WEST_VIRGINIA, WISCONSIN,
        WYOMING


    }
}
