group="com.nipr"
version="1.0.11"

plugins {
  `java-library`
}

java {
  toolchain {
    languageVersion.set(JavaLanguageVersion.of(17))
  }
}

tasks {
  test {
    useJUnitPlatform()
  }
}